.PHONY : help fetch all clean build dir bootstrap winsdk setup-debian setup-fedora veryclean docker-build docker-run docker-clean

version:=$(shell cat version)
release:=$(shell cat release)
source_release:=$(shell cat source_release)
full_version:=$(version)-$(source_release)$(shell [ $(release) -gt 1 ] && echo "-$(release)")
mozbuild=~/.mozbuild

docker_image_name=lw-android-arm

help :

	@echo "use: make [help] [all] [clean] [build] [package] [artifacts]"
	@echo ""
	@echo "  fetch     - get the latest tarball. must be done first, at least once."
	@echo ""
	@echo "  all       - Build librewolf and it's windows artifacts."
	@echo "  build     - Perform './mach build' on the extracted tarball."
	@echo "  package   - multilocale package."
	@echo ""
	@echo "  clean     - Remove output files and temporary files."
	@echo "  veryclean - Remove even more stuff."
	@echo "  dir       - just extract and patch the LW tarball."
	@echo "  bootstrap - try to set up the build environment."
	@echo "  setup-debian, setup-fedora - needed packages."
	@echo ""
	@echo "  docker-build - Run 'docker build' for" $(docker_image_name)  "image."
	@echo "  docker-run   - Run LW build using 'docker run' on" $(docker_image_name)  "image."
	@echo "  docker-clean - Remove" $(docker_image_name) "docker image."
	@echo ""

all : build package

fetch :
	wget -q -O version "https://gitlab.com/librewolf-community/browser/source/-/raw/main/version"
	wget -q -O source_release "https://gitlab.com/librewolf-community/browser/source/-/raw/main/release"
	wget -q -O "librewolf-$$(cat version)-$$(cat source_release).source.tar.gz.sha256sum" "https://gitlab.com/librewolf-community/browser/source/-/jobs/artifacts/main/raw/librewolf-$$(cat version)-$$(cat source_release).source.tar.gz.sha256sum?job=Build"
	wget -q -O "librewolf-$$(cat version)-$$(cat source_release).source.tar.gz" "https://gitlab.com/librewolf-community/browser/source/-/jobs/artifacts/main/raw/librewolf-$$(cat version)-$$(cat source_release).source.tar.gz?job=Build"
	cat "librewolf-$$(cat version)-$$(cat source_release).source.tar.gz.sha256sum"
	sha256sum -c "librewolf-$$(cat version)-$$(cat source_release).source.tar.gz.sha256sum"

clean :
	rm -rf librewolf-$(full_version) work

veryclean : clean
	rm -rf firefox-$(full_version).en-US.win64.zip librewolf-$(full_version).en-US.win64-setup.exe librewolf-$(full_version).en-US.win64-portable.zip
	rm -f "librewolf-$$(cat version)-$$(cat source_release).source.tar.gz" "librewolf-$$(cat version)-$$(cat source_release).source.tar.gz.sha256sum"
	rm -f version source_release

build : dir
	(cd librewolf-$(full_version) && ./mach build)

package : dir
	(cd librewolf-$(full_version) && ./mach package)
#	( cd librewolf-$(full_version) && echo 'Packaging... (output hidden)' && \
#	  cat browser/locales/shipped-locales | xargs ./mach package-multi-locale --locales >/dev/null )

dir : librewolf-$(full_version)

librewolf-$(full_version) : librewolf-$(full_version).source.tar.gz
	rm -rf $@
	tar xf $<
	cp -v assets/mozconfig librewolf-$(full_version)
#	(cd librewolf-$(full_version) && patch -p1 -i ../assets/tryfix-reslink-fail.patch)
#	(cd librewolf-$(full_version) && patch -p1 -i ../assets/fix-l10n-package-cmd.patch)

docker-build :
	docker build -t $(docker_image_name) - < assets/Dockerfile

docker-run :
	docker run --rm $(docker_image_name) sh -c "git pull && make fetch && make all"

docker-clean :
	docker rmi $(docker_image_name)

bootstrap : dir
	(cd librewolf-$(full_version) && ./mach --no-interactive bootstrap --application-choice=mobile_android)

setup-debian :
	apt-get -y install mercurial python3 python3-dev python3-pip curl wget dpkg-sig  libssl-dev zstd

setup-fedora :
	dnf -y install python3 curl wget zstd python3-devel python3-pip mercurial openssl-devel

